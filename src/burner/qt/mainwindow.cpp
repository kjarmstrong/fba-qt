#include <QtWidgets>
#ifdef Q_OS_MACX
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
#include "mainwindow.h"
#include "burner.h"
#include "selectdialog.h"

static int GetInput(bool bCopy)
{
    InputMake(bCopy); 						// get input
    return 0;
}

static int RunFrame(int bDraw, int bPause)
{
    static int bPrevPause = 0;
    static int bPrevDraw = 0;

    if (bPrevDraw && !bPause) {
        VidPaint(0);							// paint the screen (no need to validate)
    }

    if (!bDrvOkay) {
        return 1;
    }

    if (bPause)
    {
        GetInput(false);						// Update burner inputs, but not game inputs
        if (bPause != bPrevPause)
        {
            VidPaint(2);                        // Redraw the screen (to ensure mode indicators are updated)
        }
    }
    else
    {
        nFramesEmulated++;
        nCurrentFrame++;
        GetInput(true);					// Update inputs
    }
    if (bDraw) {
        nFramesRendered++;
        if (VidFrame()) {					// Do one frame
            AudBlankSound();
        }
    }
    else {								// frame skipping
        pBurnDraw = NULL;					// Make sure no image is drawn
        BurnDrvFrame();
    }
    bPrevPause = bPause;
    bPrevDraw = bDraw;

    return 0;
}

static int RunGetNextSound(int bDraw)
{
    if (nAudNextSound == NULL) {
        return 1;
    }
    // Render frame with sound
    pBurnSoundOut = nAudNextSound;
    RunFrame(bDraw, 0);
    return 0;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    BurnLibInit();
    createActions();
    createMenus();
    createControls();

    setWindowTitle("FB Alpha Q");

    connectActions();

    m_isRunning = false;
    VidInit();
    InputInit();
}

MainWindow::~MainWindow()
{
    BurnLibExit();
}

void MainWindow::loadGame()
{

    int ret = m_selectDlg->exec();
    if (ret != QDialog::Accepted) {
        if (m_isRunning) {
            m_timer->start();
        } else {
            m_isRunning = false;
        }
        return;
    }

    if (m_timer->isActive())
        m_timer->stop();

    AudSoundStop();

    if (bDrvOkay)
        DrvExit();

    // we need to initialize sound first
    AudSoundInit();
    AudSetCallback(RunGetNextSound);
    bAudOkay = 1;

    // now load driver
    DrvInit(m_selectDlg->selectedDriver(), false);

    if (!bDrvOkay) {
        QStringList str;
        str << BzipText.szText << BzipDetail.szText;
        QMessageBox::critical(this, "Error", str.join("\n"));
        return;
    }

    VidInit();
    AudSoundPlay();
    m_isRunning = true;

    m_actionCloseGame->setEnabled(true);

    // start emulation
    m_timer->start();
}



void MainWindow::idle()
{
    QApplication::processEvents();
    if (bDrvOkay && m_isRunning) {
        AudSoundCheck();
    }
}

void MainWindow::exitEmulator()
{
    qApp->exit(0);
}

void MainWindow::closeGame()
{
    if (bDrvOkay) {
        AudSoundStop();
        DrvExit();
        m_actionCloseGame->setEnabled(false);
        m_video->update();
    }
}

void MainWindow::setupInputInterface(QAction *action)
{
    InputExit();
    nInputSelect = action->data().toInt();
    InputInit();
}

void MainWindow::toogleMenu()
{
    if (menuBar()->isHidden()) {
        menuBar()->show();
    }
    else menuBar()->hide();
}

void MainWindow::toogleFullscreen()
{
    if (isFullScreen())
        showNormal();
    else showFullScreen();
}

void MainWindow::createMenus()
{
    m_menuGame = menuBar()->addMenu(tr("Game"));
    m_menuGame->addAction(m_actionLoadGame);
    m_menuGame->addAction(m_actionCloseGame);
    m_menuGame->addSeparator();
    m_menuGame->addAction(m_actionExitEmulator);

    m_menuInput = menuBar()->addMenu(tr("Input"));
    m_menuInputPlugin = new QMenu(tr("Plugin"), this);
    m_menuInput->addMenu(m_menuInputPlugin);
    setupInputInterfaces();

    m_menuMisc = menuBar()->addMenu(tr("Misc"));
    m_menuMisc->addAction(m_actionConfigureRomPaths);
    m_menuMisc->addAction(m_actionConfigureSupportPaths);
    m_menuMisc->addSeparator();
    m_menuMisc->addAction(m_actionToogleMenu);

    m_menuHelp = menuBar()->addMenu(tr("Help"));
    m_menuHelp->addAction(m_actionAbout);
}

void MainWindow::createControls()
{
    m_audio = QAudioInterface::get(this);
    m_video = QVideoInterface::get();
    setCentralWidget(m_video);
    resize(640, 480);
    m_selectDlg = new SelectDialog(this);
    m_supportPathDlg = new SupportDirsDialog(this);
    m_aboutDlg = new AboutDialog(this);

    m_timer = new QTimer(this);
}

void MainWindow::createActions()
{
    m_actionLoadGame = new QAction(tr("Load Game"), this);
    m_actionLoadGame->setShortcut(QKeySequence("F6"));
    m_actionCloseGame = new QAction(tr("Close Game"), this);
    m_actionCloseGame->setEnabled(false);

    m_actionConfigureRomPaths = new QAction(tr("Configure ROM paths..."), this);
    m_actionConfigureSupportPaths = new QAction(tr("Configure support paths..."), this);
    m_actionExitEmulator = new QAction(tr("Exit emulator"), this);

    m_actionToogleMenu = new QAction(tr("Toogle Menu"), this);

    m_scutToogleMenu = new QShortcut(QKeySequence(tr("F12")), this);
    m_scutToogleFullscreen = new QShortcut(QKeySequence(tr("Alt+Return")), this);

    m_scutToogleMenu->setContext(Qt::ApplicationShortcut);

    m_actionAbout = new QAction(tr("About FBA"), this);
}

void MainWindow::connectActions()
{
    connect(m_actionLoadGame, SIGNAL(triggered()), this, SLOT(loadGame()));
    connect(m_actionCloseGame, SIGNAL(triggered()), this, SLOT(closeGame()));
    connect(m_actionConfigureRomPaths, SIGNAL(triggered()), m_selectDlg, SLOT(editRomPaths()));
    connect(m_actionExitEmulator, SIGNAL(triggered()), this, SLOT(exitEmulator()));
    connect(m_actionConfigureSupportPaths, SIGNAL(triggered()), m_supportPathDlg, SLOT(exec()));
    connect(m_actionAbout, SIGNAL(triggered()), m_aboutDlg, SLOT(exec()));
    connect(m_timer, SIGNAL(timeout()), this, SLOT(idle()));
    connect(m_actionToogleMenu, SIGNAL(triggered()), this, SLOT(toogleMenu()));
    connect(m_scutToogleMenu, SIGNAL(activated()), this, SLOT(toogleMenu()));
    connect(m_scutToogleFullscreen, SIGNAL(activated()), this, SLOT(toogleFullscreen()));


}

void MainWindow::setupInputInterfaces()
{
    m_actionInputPlugins = new QActionGroup(this);
    m_actionInputPlugins->setExclusive(true);

    m_inputInterfaces = QVector<const InputInOut *>::fromStdVector(InputGetInterfaces());

    for (int i = 0; i < m_inputInterfaces.size(); i++) {
        const InputInOut *intf = m_inputInterfaces[i];
        QAction *action = new QAction(QString(intf->szModuleName), this);
        action->setCheckable(true);
        action->setChecked(i ? false : true);
        action->setData(QVariant(i));
        m_actionInputPlugins->addAction(action);
    }

    m_menuInputPlugin->addActions(m_actionInputPlugins->actions());

    connect(m_actionInputPlugins, SIGNAL(triggered(QAction*)),
            this, SLOT(setupInputInterface(QAction*)));
}
