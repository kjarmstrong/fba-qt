#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QAction>
#include <QActionGroup>
#include <QShortcut>
#include <QMenu>
#include <QThread>
#include <QVector>
#include "selectdialog.h"
#include "qvideointerface.h"
#include "qaudiointerface.h"
#include "supportdirsdialog.h"
#include "aboutdialog.h"
#include "burner.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
public slots:
    void loadGame();
    void idle();
    void exitEmulator();
    void closeGame();
    void setupInputInterface(QAction *action);
    void toogleMenu();
    void toogleFullscreen();

private:
    void createMenus();
    void createControls();
    void createActions();
    void connectActions();
    int m_game;
    bool m_isRunning;
    QVideoInterface *m_video;
    QAudioInterface *m_audio;
    QTimer *m_timer;
    SelectDialog *m_selectDlg;
    SupportDirsDialog *m_supportPathDlg;
    AboutDialog *m_aboutDlg;
    QMenu *m_menuGame;
    QMenu *m_menuMisc;
    QMenu *m_menuHelp;
    QMenu *m_menuInput;
    QMenu *m_menuInputPlugin;
    QAction *m_actionConfigureRomPaths;
    QAction *m_actionConfigureSupportPaths;
    QAction *m_actionAbout;
    QAction *m_actionLoadGame;
    QAction *m_actionExitEmulator;
    QAction *m_actionCloseGame;
    QAction *m_actionToogleMenu;
    QShortcut *m_scutToogleMenu;
    QShortcut *m_scutToogleFullscreen;

    void setupInputInterfaces();
    QVector<const InputInOut *> m_inputInterfaces;
    QActionGroup *m_actionInputPlugins;
};

#endif // MAINWINDOW_H
