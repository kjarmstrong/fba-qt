#include "burner.h"
#include "interface.h"
#include "qvideointerface.h"

#include <QDebug>
#include <QImage>
#include <QtWidgets>

static QVideoInterface *qtvideo = nullptr;
static int VidMemLen	= 0;
static int VidMemPitch	= 0;
static int VidBpp		= 0;
static int clipx		= 0;
static int clipy		= 0;
static int sizex		= 0;
static int sizey		= 0;
static unsigned char* VidMem = NULL;
QImage *qImageBuffer = nullptr;


static void copyToQImage()
{
    if (VidMem == nullptr)
        return;
    unsigned char *ps = VidMem;
    int lineLength = sizex * nBurnBpp;
    for (int y = 0; y < sizey; y++, ps += nBurnPitch) {
        unsigned char* pd = qImageBuffer->scanLine(y);
        memcpy(pd, ps, lineLength);
    }
}

static INT32 QtCanvasInit()
{
    qDebug() << "Video init...";
    qtvideo = QVideoInterface::get();

    if (BurnDrvGetFlags() & BDF_ORIENTATION_VERTICAL) {
        BurnDrvGetVisibleSize(&clipy, &clipx);
        BurnDrvGetFullSize(&sizex, &sizey);
    } else {
        BurnDrvGetVisibleSize(&clipx, &clipy);
        BurnDrvGetFullSize(&sizex, &sizey);
    }
    VidBpp		= nBurnBpp = 2;
    VidMemPitch	= sizex * VidBpp;
    VidMemLen	= sizey * VidMemPitch;
    VidMem = (unsigned char*)malloc(VidMemLen);
    SetBurnHighCol(16);

    if (qImageBuffer != nullptr)
        delete qImageBuffer;
    qImageBuffer = new QImage(sizex, sizey, QImage::Format_RGB16);

    return 0;
}

static INT32 QtCanvasExit()
{
    qDebug() << __func__;
    free(VidMem);
    delete qImageBuffer;
    qImageBuffer = nullptr;
    return 0;
}

static INT32 QtCanvasFrame(bool bRedraw)
{
    nBurnBpp=2;

    nBurnPitch = VidMemPitch;
    pBurnDraw = VidMem;

    if (bDrvOkay) {
        pBurnSoundOut = nAudNextSound;
        nBurnSoundLen = nAudSegLen;
        BurnDrvFrame();							// Run one frame and draw the screen
    }

    copyToQImage();
    nFramesRendered++;

    pBurnDraw = NULL;
    nBurnPitch = 0;
    return 0;
}

static INT32 QtCanvasPaint(INT32 bValidate)
{
    if (bValidate & 2)
        copyToQImage();
    qtvideo->repaint();
    return 0;
}

static INT32 QtCanvasImageSize(RECT* pRect, INT32 nGameWidth, INT32 nGameHeight)
{
    return 0;
}

// Get plugin info
static INT32 QtCanvasGetPluginSettings(InterfaceInfo* pInfo)
{
    return 0;
}

struct VidOut VidQtCanvas = { QtCanvasInit, QtCanvasExit, QtCanvasFrame, QtCanvasPaint,
                           QtCanvasImageSize, QtCanvasGetPluginSettings,
                           ("qt-video") };


QVideoInterface *QVideoInterface::m_onlyInstance = nullptr;

QVideoInterface::QVideoInterface(QWidget *parent) :
#if USE_OPENGL
    QGLWidget(parent)
#else
    QWidget(parent)
#endif
{
    setAttribute(Qt::WA_PaintOnScreen);
    setAutoFillBackground(false);
    m_titleScreen = QPixmap(tr(":/resource/splash.bmp"));
    resize(640, 480);
}

QVideoInterface::~QVideoInterface()
{
    m_onlyInstance = nullptr;
}

QVideoInterface *QVideoInterface::get()
{
    if (m_onlyInstance != nullptr)
        return m_onlyInstance;
    m_onlyInstance = new QVideoInterface();
    return m_onlyInstance;
}

void QVideoInterface::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
#if USE_OPENGL
    QPainter painter;
    painter.begin(this);
#else
    QPainter painter(this);
#endif
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
    //painter.setRenderHint(QPainter::Antialiasing);
    if (!bDrvOkay)
        painter.drawPixmap(rect(), m_titleScreen);
    else {
        if (qImageBuffer != nullptr) {
            painter.drawImage(rect(), *qImageBuffer);
            QPen pen(Qt::red);
            painter.setPen(pen);
            painter.drawText(30, 30, QString("%0 frames").arg(nFramesRendered));
        }
    }
#if USE_OPENGL
    painter.end();
#endif
}


