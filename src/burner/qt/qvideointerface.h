#ifndef VIDEOQT_H
#define VIDEOQT_H

#define USE_OPENGL 1

#if USE_OPENGL
#include <QGLWidget>
#else
#include <QWidget>
#endif
#if USE_OPENGL
class QVideoInterface : public QGLWidget
#else
class QVideoInterface : public QWidget
#endif
{
    Q_OBJECT

    QVideoInterface(QWidget *parent=0);
    static QVideoInterface *m_onlyInstance;
public:
    ~QVideoInterface();

    static QVideoInterface *get();
protected:
    void paintEvent(QPaintEvent *event);
    QPixmap m_titleScreen;
};

#endif // VIDEOQT_H
