#ifndef QXCBINPUTINTERFACE_H
#define QXCBINPUTINTERFACE_H

#include <QAbstractNativeEventFilter>
#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>

class QXcbInputInterface : public QAbstractNativeEventFilter
{
    QXcbInputInterface();
    static QXcbInputInterface *m_onlyInstance;
    char m_keys[256];
    xcb_key_symbols_t *m_xcbSymbols;
    xcb_connection_t *m_xcbConnection;
    bool m_isInitialized;
public:
    ~QXcbInputInterface();
    void snapshot(char *buffer, int keys=256);
    void install();
    void uninstall();
    static QXcbInputInterface *get();
    virtual bool nativeEventFilter(const QByteArray &eventType, void *message, long *);

};

#endif // QXCBINPUTINTERFACE_H
